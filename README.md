#How use the SE:

You can easily use the SE after following this steps:

1.Import the SE's file to your java game.

2.Create a new class and in this class make sure you implement DisplaySystem and extend Display.

Example:

	public class Game extends Display implements DisplaySystem{
	@Override
	public void Start() {
                   dSystem = this;//it is mandatory
                    }
	@Override
	public void Draw(GraphicsDraw g) {

                    }
	@Override
	public void PostProcessing() {

                    }
	@Override
	public void Run() {

                    }

	}
	
3.Create an other new class that had the main method , and create a  new object of your first class.

Exemple:

	public class Main{

	public static void main(String[] args){
	Game g = new Game();
	g.Start();
	g.DisplayGame();
	}

	}

	public class Game extends Display implements DisplaySystem{
	@Override
	public void Start() {
                   dSystem = this;
                    }
	@Override
	public void Draw(GraphicsDraw g) {

                    }
	@Override
	public void PostProcessing() {

                    }
	@Override
	public void Run() {

                    }

	}
	
And finally ,it's done.
